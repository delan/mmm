// keep in sync with theme.json
const THEME = {
    "waterColor": {
        "internal": {
            "day": [118,119,106],
            "night": [118,119,106]
        },
    },
};

const stats = {};

function convert(path, kind = "????") {
    const result = [];
    for (const seg of path.getPathData({normalize: true})) {
        switch (seg.type) {
            case "Z":
                break;
            case "M":
            case "L":
                result.push([seg.values[0], -seg.values[1]]);
                break;
            default:
                throw new Error(`unexpected path command type ${seg.type}`);
        }
    }
    if (!(kind in stats))
        stats[kind] = [];
    stats[kind].push(result.length);
    return result;
}

function $(selector, ...rest) {
    return document.querySelector(selector, ...rest);
}

const out = $("#out");
const err = $("#err");
out.value = err.value = "";

try {
    const result = {
        "id":"azabani/per",
        "customName":"Perth",
        "customCountryName":"Australia",
        "customDescription":"(v2.2) Reimagine the Transperth Trains network in this sprawling remote city down under.",
        "customLocalName": "Boorloo",
        "customLocalNameLocale": "nys",
        "lineCount": 7,
        "crossingStyle": "Bridge",
        "audioLoadoutId": "melbourne",
        "theme":"SmartRider",
        "debugShowCityAreas": false,
        "origin": [0,0],
        "zoom": {
            "start": 1.25,
            "end": 0.34375,
            "earlyZoom": 0.875,
            "lateZoom": 0.75,
            "delay": 1,
            "duration": 42
        },
        "startArea":{
            "rightBottom": [0,0],
            "leftTop": [0,0]
        },
        "passengerSpawnScale": 1.0625,
        "stationSeparationScale": 0.75,
        "stationCapacity": 6,
        "interchangeCapacity": 18,
        "trainDefinition": "WAGR",
        "transitions": [
            {
                "points": convert($("#t0"), "tran"),
                "id": "menu-game"
            },
            {
                "points": [[0,0],[0,0]],
                "id": "menu-pause"
            }
        ],
        "preview": {
            "window": {
                "position": [-540,-700],
                "size": 1300
            },
            "_stations": [
                {"tag": "Fremantle", "type": "triangle", "position": convert($("#s"))[0]},
                {"tag": "Perth", "type": "circle", "position": convert($("#s"))[1]},
                {"tag": "Guildford", "type": "triangle", "position": convert($("#s"))[2]},
                {"tag": "Armadale", "type": "square", "position": convert($("#s"))[3]},
            ],
            "_lines": [
                {
                    "index": 1,
                    "name": "Fremantle to Guildford",
                    "links": [
                        {"station": "Fremantle", "stops": [ {"direction": "NORTHEAST", "platform": 0} ] },
                        {"station": "Perth", "stops": [ {"direction": "SOUTHWEST", "platform": 0}, {"direction": "NORTHEAST", "platform": 0} ] },
                        {"station": "Guildford", "stops": [ {"direction": "SOUTHWEST", "platform": 0} ] },
                    ],
                },
                {
                    "index": 0,
                    "name": "Perth to Armadale",
                    "links": [
                        {"station": "Perth", "stops": [ {"direction": "SOUTHEAST", "platform": 0} ] },
                        {"station": "Armadale", "stops": [ {"direction": "NORTHWEST", "platform": 0} ] },
                    ],
                },
            ],
        },
        "stationSpawns": {
            "cooldown": 0,
            "schedule": [
                {
                    "numDays": 11,
                    "randomness": 0.25,
                    "types": ["CIRCLE","CIRCLE","TRIANGLE","TRIANGLE","TRIANGLE","SPECIAL"]
                },
                {
                    "numDays": 11,
                    "randomness": 0.50,
                    "types": ["CIRCLE","CIRCLE","CIRCLE","TRIANGLE","TRIANGLE","SQUARE"]
                },
                {
                    "numDays": 11,
                    "randomness": 0.75,
                    "types": ["CIRCLE","CIRCLE","TRIANGLE","SQUARE","SQUARE","SPECIAL"]
                }
            ],
            "special": ["PENTAGON", "DIAMOND", "STAR", "CROSS", "WEDGE", "EGG", "GEM"]
        },
        "upgrades": {
            "initial": [
                "Line",
                "Line",
                "Line",
                "Locomotive",
                "Locomotive",
                "Locomotive",
                "Crossing",
                "Crossing",
                "Crossing"
            ],
            "numOptions": [2,2],
            "unlocks": [
                [
                    {"type":"Locomotive", "weight": 1, "count": 1, "week": 2}
                ],
                [
                    {"type":"Line", "weight": 4, "count": 1, "max": 4},
                    {"type":"Carriage", "weight": 3, "count": 2},
                    {"type":"Locomotive", "weight": 2, "count": 1},
                    {"type":"Crossing", "weight": 2, "count": 2},
                    {"type":"Interchange", "weight": 2, "count": 1}
                ]
            ]
        },
        "stations": [
            {"tag":"Perth", "type": "circle", "position": convert($("#s"))[1]},
            {"tag":"Triangle0", "type": "triangle"},
            {"tag":"Square0", "type": "square"}
        ],
        "lines": [
            {
                "name": "1",
                "maxTrains": 5,
                "allowedTrains": ["Locomotive", "Tram", "Shinkansen", "Carriage", "Crossing"],
                "startAssets": ["Locomotive"]
            },
            {
                "name": "2",
                "maxTrains": 5,
                "allowedTrains": ["Locomotive", "Tram", "Shinkansen", "Carriage", "Crossing"],
                "startAssets": ["Locomotive"]
            },
            {
                "name": "3",
                "maxTrains": 5,
                "allowedTrains": ["Locomotive", "Tram", "Shinkansen", "Carriage", "Crossing"]
            },
            {
                "name": "4",
                "maxTrains": 5,
                "allowedTrains": ["Locomotive", "Tram", "Shinkansen", "Carriage", "Crossing"]
            },
            {
                "name": "5",
                "maxTrains": 5,
                "allowedTrains": ["Locomotive", "Tram", "Shinkansen", "Carriage", "Crossing"]
            },
            {
                "name": "6",
                "maxTrains": 5,
                "allowedTrains": ["Locomotive", "Tram", "Shinkansen", "Carriage", "Crossing"]
            },
            {
                "name": "7",
                "maxTrains": 5,
                "allowedTrains": ["Locomotive", "Tram", "Shinkansen", "Carriage", "Crossing"]
            }
        ],
        "obstacles": [
            {
                "points": convert($("#o0"), "obst"),
                "note": "back",
                "visual": true,
                "inverted": true,
                "decoration": true,
                "inPreview": false,
                "cornerRadius": -1
            },
            {
                "points": convert($("#o1"), "obst"),
                "note": "swan",
                "visual": true,
                "inverted": false,
                "decoration": false,
                "inPreview": true,
                "cornerRadius": -1
            },
            {
                "points": convert($("#o2"), "obst"),
                "note": "Lake Joondalup",
                "visual": true,
                "inverted": false,
                "decoration": false,
                "inPreview": true,
                "alpha": 176,
                "cornerRadius": -1
            },
            {
                "points": convert($("#o3"), "obst"),
                "note": "Avocet Island",
                "visual": true,
                "inverted": true,
                "decoration": false,
                "inPreview": true,
                "cornerRadius": -1
            },
            {
                "points": convert($("#o4"), "obst"),
                "note": "Garden Island",
                "visual": true,
                "inverted": true,
                "decoration": false,
                "inPreview": true,
                "alpha": 144,
                "outlineColor": THEME.waterColor.internal,
                "cornerRadius": -1
            },
            {
                "points": convert($("#o5"), "obst"),
                "note": "Carnac Island",
                "visual": true,
                "inverted": true,
                "decoration": false,
                "inPreview": true,
                "alpha": 144,
                "outlineColor": THEME.waterColor.internal,
                "cornerRadius": -1
            },
            {
                "points": convert($("#o6"), "obst"),
                "note": "Rottnest Island",
                "visual": true,
                "inverted": true,
                "decoration": false,
                "inPreview": true,
                "alpha": 144,
                "outlineColor": THEME.waterColor.internal,
                "cornerRadius": -1
            },
            {
                "note": "origin",
                "points": convert($("#oo"), "obst"),
                "visual": false,
                "inverted": false,
                "decoration": true,
                "inPreview": false,
                "cornerRadius": -1
            }
        ],
        "cityAreas": [
            {
                "label": "Airport",
                "density": 0.75,
                "stationSpawns": [
                    {"type": "TRIANGLE", "weight": 0.25, "maximum": -1, "activeDay": -1},
                    {"type": "SPECIAL", "weight": 0.75, "maximum": -1, "activeDay": -1}
                ],
                "paths": [{"points": convert($("#aair"), "area")}]
            },
            {
                "label": "Vincent",
                "density": 1.25,
                "stationSpawns": [
                    {"type": "CIRCLE", "weight": 0.34375, "maximum": -1, "activeDay": -1},
                    {"type": "TRIANGLE", "weight": 0.28125, "maximum": -1, "activeDay": -1},
                    {"type": "SQUARE", "weight": 0.21875, "maximum": -1, "activeDay": -1},
                    {"type": "SPECIAL", "weight": 0.15625, "maximum": -1, "activeDay": -1}
                ],
                "paths": [{"points": convert($("#avin"), "area")}]
            },
            {
                "label": "Maylands",
                "density": 1.21875,
                "stationSpawns": [
                    {"type": "CIRCLE", "weight": 0.34375, "maximum": -1, "activeDay": -1},
                    {"type": "TRIANGLE", "weight": 0.28125, "maximum": -1, "activeDay": -1},
                    {"type": "SQUARE", "weight": 0.21875, "maximum": -1, "activeDay": -1},
                    {"type": "SPECIAL", "weight": 0.15625, "maximum": -1, "activeDay": -1}
                ],
                "paths": [{"points": convert($("#amay"), "area")}]
            },
            {
                "label": "Subiaco",
                "density": 1.1875,
                "stationSpawns": [
                    {"type": "CIRCLE", "weight": 0.34375, "maximum": -1, "activeDay": -1},
                    {"type": "TRIANGLE", "weight": 0.28125, "maximum": -1, "activeDay": -1},
                    {"type": "SQUARE", "weight": 0.21875, "maximum": -1, "activeDay": -1},
                    {"type": "SPECIAL", "weight": 0.15625, "maximum": -1, "activeDay": -1}
                ],
                "paths": [{"points": convert($("#asub"), "area")}]
            },
            {
                "label": "Victoria Park",
                "density": 1.125,
                "stationSpawns": [
                    {"type": "CIRCLE", "weight": 0.34375, "maximum": -1, "activeDay": -1},
                    {"type": "TRIANGLE", "weight": 0.28125, "maximum": -1, "activeDay": -1},
                    {"type": "SQUARE", "weight": 0.21875, "maximum": -1, "activeDay": -1},
                    {"type": "SPECIAL", "weight": 0.15625, "maximum": -1, "activeDay": -1}
                ],
                "paths": [{"points": convert($("#avic"), "area")}]
            },
            {
                "label": "Rivervale",
                "density": 1.125,
                "stationSpawns": [
                    {"type": "CIRCLE", "weight": 0.34375, "maximum": -1, "activeDay": -1},
                    {"type": "TRIANGLE", "weight": 0.28125, "maximum": -1, "activeDay": -1},
                    {"type": "SQUARE", "weight": 0.21875, "maximum": -1, "activeDay": -1},
                    {"type": "SPECIAL", "weight": 0.15625, "maximum": -1, "activeDay": -1}
                ],
                "paths": [{"points": convert($("#ariv"), "area")}]
            },
            {
                "label": "Fremantle",
                "density": 1.09375,
                "stationSpawns": [
                    {"type": "CIRCLE", "weight": 0.34375, "maximum": -1, "activeDay": -1},
                    {"type": "TRIANGLE", "weight": 0.28125, "maximum": -1, "activeDay": -1},
                    {"type": "SQUARE", "weight": 0.21875, "maximum": -1, "activeDay": -1},
                    {"type": "SPECIAL", "weight": 0.15625, "maximum": -1, "activeDay": -1}
                ],
                "paths": [{"points": convert($("#afre"), "area")}]
            },
            {
                "label": "Joondalup",
                "density": 1.09375,
                "stationSpawns": [
                    {"type": "CIRCLE", "weight": 0.34375, "maximum": -1, "activeDay": -1},
                    {"type": "TRIANGLE", "weight": 0.28125, "maximum": -1, "activeDay": -1},
                    {"type": "SQUARE", "weight": 0.21875, "maximum": -1, "activeDay": -1},
                    {"type": "SPECIAL", "weight": 0.15625, "maximum": -1, "activeDay": -1}
                ],
                "paths": [{"points": convert($("#ajoo"), "area")}]
            },
            {
                "label": "Rockingham",
                "density": 1.0625,
                "stationSpawns": [
                    {"type": "CIRCLE", "weight": 0.34375, "maximum": -1, "activeDay": -1},
                    {"type": "TRIANGLE", "weight": 0.28125, "maximum": -1, "activeDay": -1},
                    {"type": "SQUARE", "weight": 0.21875, "maximum": -1, "activeDay": -1},
                    {"type": "SPECIAL", "weight": 0.15625, "maximum": -1, "activeDay": -1}
                ],
                "paths": [{"points": convert($("#aroc"), "area")}]
            },
            {
                "label": "Mandurah",
                "density": 1.0625,
                "stationSpawns": [
                    {"type": "CIRCLE", "weight": 0.34375, "maximum": -1, "activeDay": -1},
                    {"type": "TRIANGLE", "weight": 0.28125, "maximum": -1, "activeDay": -1},
                    {"type": "SQUARE", "weight": 0.21875, "maximum": -1, "activeDay": -1},
                    {"type": "SPECIAL", "weight": 0.15625, "maximum": -1, "activeDay": -1}
                ],
                "paths": [{"points": convert($("#aman"), "area")}]
            },
            {
                "label": "Avocet",
                "density": 1.0625,
                "stationSpawns": [
                    {"type": "CIRCLE", "weight": 0.34375, "maximum": -1, "activeDay": -1},
                    {"type": "TRIANGLE", "weight": 0.28125, "maximum": -1, "activeDay": -1},
                    {"type": "SQUARE", "weight": 0.21875, "maximum": -1, "activeDay": -1},
                    {"type": "SPECIAL", "weight": 0.15625, "maximum": -1, "activeDay": -1}
                ],
                "paths": [{"points": convert($("#aavo"), "area")}]
            },
            {
                "label": "Ellenbrook",
                "density": 1.0625,
                "stationSpawns": [
                    {"type": "CIRCLE", "weight": 0.34375, "maximum": -1, "activeDay": -1},
                    {"type": "TRIANGLE", "weight": 0.28125, "maximum": -1, "activeDay": -1},
                    {"type": "SQUARE", "weight": 0.21875, "maximum": -1, "activeDay": -1},
                    {"type": "SPECIAL", "weight": 0.15625, "maximum": -1, "activeDay": -1}
                ],
                "paths": [{"points": convert($("#aell"), "area")}]
            },
            {
                "label": "NOR",
                "density": 0.8125,
                "stationSpawns": [
                    {"type": "CIRCLE", "weight": 0.34375, "maximum": -1, "activeDay": -1},
                    {"type": "TRIANGLE", "weight": 0.28125, "maximum": -1, "activeDay": -1},
                    {"type": "SQUARE", "weight": 0.21875, "maximum": -1, "activeDay": -1},
                    {"type": "SPECIAL", "weight": 0.15625, "maximum": -1, "activeDay": -1}
                ],
                "paths": [{"points": convert($("#anor"), "area")}]
            },
            {
                "label": "SOR",
                "density": 0.8125,
                "stationSpawns": [
                    {"type": "CIRCLE", "weight": 0.34375, "maximum": -1, "activeDay": -1},
                    {"type": "TRIANGLE", "weight": 0.28125, "maximum": -1, "activeDay": -1},
                    {"type": "SQUARE", "weight": 0.21875, "maximum": -1, "activeDay": -1},
                    {"type": "SPECIAL", "weight": 0.15625, "maximum": -1, "activeDay": -1}
                ],
                "paths": [{"points": convert($("#asor"), "area")}]
            }
        ]
    };
    err.value += `PATH STATS\n`;
    let total = 0;
    for (const kind in stats) {
        const subtotal = stats[kind].reduce((r,x) => r + x, 0);
        err.value += `• ${kind} (${subtotal}): ${stats[kind].join(" ")}\n`;
        total += subtotal;
    }
    err.value += `• grand total: ${total} points\n`;
    out.value = JSON.stringify(result, null, 4);
    out.select();
    requestAnimationFrame(() => {
        out.scrollTop = 0;
    });
} catch (e) {
    err.value = `${e}\n${e.stack}`;
}

// interactive stuff
const root = document.documentElement;
const props = document.querySelector("#props");
let zoom = 1, x = document.documentElement.clientWidth / 2, y = document.documentElement.clientHeight / 2;
let clicking = false;
let dragging = false;
let selected = null;
root.style.setProperty("--zoom", zoom);
root.style.setProperty("--panX", `${x}px`);
root.style.setProperty("--panY", `${y}px`);
updateProps();
addEventListener("wheel", event => {
    if (event.target.closest("#ui"))
        return;
    if (event.deltaY > 0) {
        const newZoom = zoom / 2;
        const ratio = 1 - newZoom / zoom;
        x += (event.clientX - x) * ratio;
        y += (event.clientY - y) * ratio;
        zoom = newZoom;
        root.style.setProperty("--zoom", zoom);
        root.style.setProperty("--panX", `${x}px`);
        root.style.setProperty("--panY", `${y}px`);
    } else if (event.deltaY < 0) {
        const newZoom = zoom * 2;
        const ratio = 1 - newZoom / zoom;
        x += (event.clientX - x) * ratio;
        y += (event.clientY - y) * ratio;
        zoom = newZoom;
        root.style.setProperty("--zoom", zoom);
        root.style.setProperty("--panX", `${x}px`);
        root.style.setProperty("--panY", `${y}px`);
    }
});
addEventListener("mousedown", event => {
    if (event.target.closest("#ui"))
        return;
    clicking = true;
    dragging = false;
    root.classList.add("clicking");
});
addEventListener("mouseup", event => {
    clicking = false;
    root.classList.remove("clicking");
});
addEventListener("mousemove", event => {
    if (!clicking)
        return;
    dragging = true;
    event.preventDefault();
    x += event.movementX;
    y += event.movementY;
    root.style.setProperty("--panX", `${x}px`);
    root.style.setProperty("--panY", `${y}px`);
});
addEventListener("click", event => {
    if (event.target.closest("#ui") || dragging)
        return;
    if (selected != null) {
        selected.classList.remove("selected");
        selected = null;
    }
    if (event.target.nodeName == "path") {
        root.classList.add("selecting");
        event.target.classList.add("selected");
        selected = event.target;
    } else {
        root.classList.remove("selecting");
    }
    updateProps();
});
function updateProps() {
    if (selected == null) {
        props.value = "nothing selected";
        return;
    }
    props.value = `#${selected.id}`;
    if (selected.closest("#areas"))
        props.value += "\nin #areas";
    if (selected.closest("#obstacles"))
        props.value += "\nin #obstacles";
    if (selected.closest("#stations"))
        props.value += "\nin #stations";
    if (selected.closest("#transitions"))
        props.value += "\nin #transitions";
}
